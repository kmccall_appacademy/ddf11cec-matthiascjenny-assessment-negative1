# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result = []
  i = 1
  n = nums[0] + 1
  while i < nums.length
    if nums[i] > n
      result.push(n)
      n += 1
    else
      n = nums[i] + 1
      i += 1
    end
  end
  return result
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  i = 0
  result = 0
  while i < binary.length
    result += binary[i].to_i * 2**(binary.length - 1 - i)
    i += 1
  end
  return result
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hsh = Hash.new
    self.each do |k,v|
      if prc.call(k,v)
        hsh[k] = v
      end
    end
    return hsh
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.
  def my_merge(hash, &prc)
    if block_given?
      self.each do |k_s,v_s|
        hash.each do |k_h,v_h|
          if k_s == k_h
            self[k_s] = prc.call(k_s,v_s,v_h)
            break
          end
        end
      end
      hash.each do |k,v|
        if self[k] == nil
          self[k] = v
        end
      end
      return self
    else
      hash.each { |k,v| self[k] = v }
      return self
    end
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  if n >= 0
    if n == 0
      return 2
    elsif n == 1
      return 1
    else
      arr = [2, 1]
      (n-1).times do
        arr.push(arr[-1]+arr[-2])
      end
      return arr[-1]
    end
  else
    if n == -1
      return -1
    elsif n == -2
      return 3
    else
      arr = [3, -1]
      (n.abs-2).times do
        arr.unshift(arr[1]-arr[0])
      end
      return arr[0]
    end
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.
#require 'byebug'
def longest_palindrome(string)
#  debugger
  longest = 0
  i = 0
  j = 2
  while i + 2 < string.length
    while j < string.length
      if palindrome?(string[i..j]) && 1 + j - i > longest
        longest = 1 + j - i
        j += 1
      else
        j += 1
      end
    end
    i += 1
    j = i + 2
  end
  if longest == 0
    return false
  else
    return longest
  end
end

def palindrome?(string)
  arr = string.split("")
  while arr.length > 1
    if arr[0] == arr[-1]
      arr.shift
      arr.pop
    else
      return false
    end
  end
  return true
end
